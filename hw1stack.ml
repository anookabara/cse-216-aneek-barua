let start k = k []
let push stack x k = k (x :: stack)
let pop (_ :: stack’) k = k stack’
let add (a :: b :: stack’) k = k (a + b :: stack’)
let mult (a :: b :: stack’) k = k (a * b :: stack’)
let kpop (a :: b :: stack’) k = if (k mod 2 = 0) (a :: b :: stack’)
let halt (x :: _) = 