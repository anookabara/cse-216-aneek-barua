type bool_expr =
  | Lit of string
  | Not of bool_expr
  | And of bool_expr * bool_expr
  | Or of bool_expr * bool_expr
  ;;

let rec logicalEval term1 x term2 y expr = match expr with
  | Lit(i) -> if i = x then term1 else term2
  | And(i, j) -> logicalEval term1 x term2 y i && logicalEval term1 x term2 y j
  | Not(j) -> not(logicalEval term1 x term2 y j)
  | Or(i, j) -> logicalEval term1 x term2 y i || logicalEval term1 x term2 y j


let truth_table a b expr =
  [(true,  true,  logicalEval true a true b expr);
  (true,  false, logicalEval true a false b expr);
  (false, true,  logicalEval false a true b expr);
  (false, false, logicalEval false a false b expr) ];;
