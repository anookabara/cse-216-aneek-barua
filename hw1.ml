(* Here is the first method which
recursively traces through numbers
and multiplies. In this case, there 
are a few universal rules, specifically
that any number raised to 0 is 1 and any
number raised to 1 is itself. There's
also that 0 to any power is 0 and that
1 to any power is 1.*)
let rec pow a b = 
  if a = 0
  then 0
  else if a = 1
  then 1
  else if b = 0
  then 1
  else if b = 1
  then a
  else 
  a * pow a (b - 1);;

let rec compress (finalList : 'a list) : 'a list = match finalList with
  | i::(j::_ as recur) -> 
    if i = j 
      then compress recur
    else i::compress recur
  | _ -> finalList;;

(*let rec remove_if*) 

let rec slice list i j = 
  if (list=[])
    then [] 
  else if (i > j)
    then []
  else if (i > (List.length list))
    then []
  else slice_helper list i j []
and slice_helper oldList i j newList = match oldList with
  | [] -> newList
  | h::t -> 
    if i > 0 
      then slice_helper t (i - 1) (j - 1) newList
    else if not (i = j)
      then slice_helper t i (j - 1) (newList @ [h]) 
    else newList;;

let rec equivs cmp list = match list with
  | [] -> [[]]
  | h::t -> 
    equivs_helper cmp h list [] [] [[]]
and equivs_helper cmp compVal oldList remainList subList newList = match oldList with
  | [] -> 
    if remainList = [] 
      then (List.hd newList @ [subList])
    else equivs_helper cmp (List.hd remainList) remainList [] [] ([List.hd newList @ [subList]] @ [])
  | h::t -> 
    if (cmp compVal h) 
      then equivs_helper cmp compVal t remainList (subList @ [h]) newList
    else equivs_helper cmp compVal t (remainList @ [h]) subList newList;;

let rec goldbachpair x = 
  if x < 2 
    then (x,0)
  else if (x mod 2 = 1) 
    then (x,0)
  else goldbachpair_helper x (x-2)
and goldbachpair_helper x i = 
  if (isPrime 2 i) 
    then if (isPrime 2 (x - i)) 
      then ((x - i), i)
    else goldbachpair_helper x (i - 1)
  else goldbachpair_helper x (i - 1)
and isPrime n i =
  if n < i 
    then if (i mod n = 0) 
      then false
    else isPrime (n + 1) i
  else true;;

let rec equiv_on f g list = match list with
  | [] -> true
  | x::xs -> 
    if ((f x) = (g x)) 
     then equiv_on f g xs
    else false;;

let rec pairwisefilter cmp list = match list with
  | [] -> []
  | h::t -> pairwisefilter_helper cmp h t []
and pairwisefilter_helper cmp first remainList newList = match remainList with
  | [] -> (newList @ [first])
  | [x] -> (newList @ [(cmp first x)])
  | h::t -> pairwisefilter_helper cmp (List.hd t) (List.t1 t) (newList @ [(cmp first h)]);;

let rec polynomial = function
  | [] -> fun x -> 0
  | h::t -> fun c -> polynomial_helper x h t
and polynomial_helper x term remainList = 
  if remainList = [] 
    then (fst term)* (pow x (snd term))
  else (fst term)*(pow x (snd term)) * polynomial_helper x (List.hd remainList) (List.t1 remainList)

let rec powerset = function
  | [] -> [[]]
  | x::xs -> 
    let ls = sublists xs in
      List.map (fun l -> x::l) ls @ ls;;